# SafeEdits

Mediawiki extension. Backup of unsaved articles.
It has lots of dependencies.

## Description

* Version 0.2
* Save articles in regular intervals. 
* For WikiEditor only.
* Offer list of unsaved versions. See the "Restore" button under the WikiEditor.
* There is an extension [SaferEdit](https://www.mediawiki.org/wiki/Extension:SaferEdit) for this purpose. But it was written for [Bluespice](https://en.wiki.bluespice.com/) and I wasn't able to run it on Mediawiki.

## Installation

* Make sure you have MediaWiki 1.36+ installed.
* Bootstrap 5 required.
* WikiEditor present.
* Download and place the extension to your /extensions/ folder.
* Set interval for saving in _extension.json_.
* Add the following code to your LocalSettings.php:

``` php
wfLoadExtension('SafeEdits');
```

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Release Notes

### 0.1

* https://javascript.info/indexeddb
* Ulož backup pouze pokud došlo ke změně.

### 0.2

* Style converted to Bootstrap 5.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2023 First Faculty of Medicine, Charles University
