(function (mw, $) {
    
    // get values from extension
    let interval = $("#safeedits").data("interval");
    let expiration = $("#safeedits").data("expiration");
    let pageid = $("#safeedits").data("pageid");

    // remove data element
    $("#safeedits").remove();

    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    // DON'T use "var indexedDB = ..." if you're not in a function.
    if (!window.indexedDB) {
        console.log("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.");
        return true;
    }

    // display button for backups menu
    var btn = "<span aria-disabled='false' class='oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-buttonElement oo-ui-buttonElement-framed oo-ui-labelElement oo-ui-buttonInputWidget'>";
    btn += "<input type='button' data-bs-target='#seModal' data-bs-toggle='modal' tabindex='5' aria-disabled='false' id='showBackup' class='oo-ui-inputWidget-input oo-ui-buttonElement-button' value='";
    btn += mw.message("safeedits-button-title").text() + "' title='" + mw.message("safeedits-button-legend").text() + "'></span>";
    $("#wpDiffWidget" ).after(btn);

    // add modal (hidden)
    modal = "<div id='seModal' class='modal fade' tabindex='-1' role='dialog' aria-hidden='true'>\n";
    modal += "<div class='modal-dialog modal-md'>\n";		
    modal += "<div class='modal-content p-2'>\n";
    modal += "<div class='modal-header'>\n";
    modal += "<div><h3>" + mw.message("safeedits-modal-title").text() + "</h3></div>\n";
    modal += "<button type='button' class='close border-0 h3 bg-white mt-1' data-bs-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\n";
    modal += "</div>\n"; // end of header
    modal += "<div class='modal-body'>\n";
    modal += "<div class='mt-2 mb-3'>" + mw.message("safeedits-modal-legend").text() + "</div>";
    // list of backups
    modal += "<div id='backupList' class='list-group'>\n";
    modal += "</div>\n";
    modal += "</div>\n"; // end of body
    modal += "</div>\n</div>\n</div>\n";
    $("#bodyContent").prepend(modal);
    $('#seModal').on('show.bs.modal', function (e) {
        // update modal content
        $(".restoreSel").off('click');
        $("#backupList").empty();
        let db = openRequest.result;
        getAllKeysQuery(db, (data = []) => {
            for (const rw of data) {
                let kp = rw.split('-');
                let d = new Date(parseInt(kp[0]));
                let dt = d.getFullYear() + '-' + addLeadingZero(d.getMonth()) + '-' + addLeadingZero(d.getDate()) + ' ';
                dt += addLeadingZero(d.getHours()) + ':' + addLeadingZero(d.getMinutes()) + ':' + addLeadingZero(d.getSeconds());
                if(pageid == kp[1]) {
                    getContentQuery(db, rw, content => {
                        $("#backupList").prepend("<div class='list-group-item restoreSel' data-key='" + rw + "'><span role='button'>" + dt + "</span></div>");
                        
                        $("*[data-key='" + rw + "']").find("span").click(function(){
                            if($(this).parent().find(".seTooltip").length == 0) {
                                var preview = "<div class='seTooltip' style='border:1px solid #dfdfdf;padding:10px;margin-top:10px;background-color:#fff3a5;'>";
                                preview += "<div class='mb-1 text-right mt-1'><button class='seRestore btn btn-sm btn-primary me-1'>" + mw.message("safeedits-restore").text() + "</button>";
                                preview += "<button class='seClose btn btn-sm btn-primary'>" + mw.message("safeedits-close").text() + "</button></div>";
                                preview += content.value;
                                preview += "</div>";
                                $(preview).insertAfter(this);
                                $(this).parent().find(".seRestore").click(function(){
                                    $("#wpTextbox1").val(content.value);
                                    $("#wpTextbox1").change();
                                    $('#seModal').modal('hide');
                                });
                                $(this).parent().find(".seClose").click(function(){
                                    $(this).parent().parent().toggle();
                                });
                            }
                            else {
                                $(this).parent().find(".seTooltip").toggle();
                            }
                        });
                    });
                }
            }
        });

    })

    let newContent = $("#wpTextbox1").val();
    $("#wpTextbox1").on('input change propertychange', function() { 
        if(this.value.length){
            //console.log('newlen-onchange:' + this.value.length);
            newContent = this.value;
        }
    });

    //console.log(interval);
    //console.log(expiration);
    //console.log(pageid);

    let openRequest = indexedDB.open("wbackupDb", 1);

    openRequest.onupgradeneeded = function() {
        // triggers if the client had no database
        let db = openRequest.result;
        if (!db.objectStoreNames.contains('wbackup')) { // if there's no "books" store
            db.createObjectStore('wbackup', {keyPath: 'key'}); // create it
        }
    };

    openRequest.onerror = function() {
        console.error("Error", openRequest.error);
    };

    openRequest.onsuccess = function() {
        let db = openRequest.result;
        let now = new Date().getTime();

        // delete old backups and get last key
        let lastKey = undefined;
        getAllKeysQuery(db, (data = []) => {
            // remove all transactions older then "expiration" hours
            //console.log('received %d rows of data', data.length);
            for (const row of data) {
                // row format: timestamp-pageid-contentlength
                let keyParts = row.split('-');
                let backupTimestamp = keyParts[0];
                let backupPageid = keyParts[1];

                if(backupTimestamp < (now - expiration * 60 * 60 * 1000)) {
                    //console.log('object old - DELETE IT:', row);
                    let tx = db.transaction('wbackup', "readwrite");
                    let store = tx.objectStore('wbackup');
                    store.delete(row);
                }
                else if(pageid == backupPageid) {
                    //console.log('object recent - NO ACTION:', row);
                    lastKey = row;
                }
            }

            backup(db, pageid, lastKey);
        });
    };

    function getAllKeysQuery(db, gakCallbackFunction) {
        let tx = db.transaction('wbackup', "readwrite");
        let store = tx.objectStore('wbackup');
        let request = store.getAllKeys();
        request.onsuccess = event => {
            // denote the array of objects with a variable
            // here, event.target is === to request, can use either one
            const data = event.target.result;
            // pass the data to the callback function so that caller can
            // access it
            gakCallbackFunction(data);
        };
    }

    function getContentQuery(db, keyToSearch, gcCallbackFunction) {
        let tx = db.transaction('wbackup', "readwrite");
        let store = tx.objectStore('wbackup');
        let request = store.get(keyToSearch);
        request.onsuccess = event => {
            const data = event.target.result;
            gcCallbackFunction(data);
        };
    }

    function backup(db, pageid, lastKey) {

        let nowTS = new Date().getTime();
        let bckpContentLength = 0;
        if(lastKey != undefined) {
            key = lastKey.split('-');
            bckpContentLength = key[2];
        }

        //console.log('bckplen: ' + bckpContentLength);
        //console.log('newlen: ' + newContent.length);

        setTimeout(function() {
            let newKey = nowTS + '-' + pageid + '-' + newContent.length;
            if(lastKey == undefined || bckpContentLength != newContent.length) {
                let tx = db.transaction('wbackup', "readwrite");
                let store = tx.objectStore('wbackup');
                const request = store.add({key: newKey, value: newContent});
                request.onsuccess = event => {
                    // add record
                    //console.log("Record added to the store", request.result);
                    res = backup(db, pageid, newKey);
                    if(!res) return false;
                };
                request.onerror = event => {
                    console.log("Error:", request.error);
                    return false;
                };
            }
            else {
                //console.log("No change in editor since last version");
                //clearTimeout(intHandler);
                res = backup(db, pageid, lastKey);
                if(!res) return false;
            }
        }, interval * 60 * 1000); // interval * 60 * 1000
    }

}(mediaWiki, jQuery));

function addLeadingZero(str) {
    return ("0" + str).slice(-2);
}
