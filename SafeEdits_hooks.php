<?php

class SafeEditsHooks {

       /**
        * Repeat editors's windows backup in regular intervals
        * @param object $out: instance of OutputPage
        * @param object $skin: instance of Skin, unused
        */
        public static function startBackup( &$out, &$skin ) {

            // don't fire at articles
            if($out->isArticle()) return true;

            // only allowed namespaces
            $config = $out->getConfig();
            $title = $out->getTitle();
            $ns = $title->getNamespace();
            $namespaces = $config->get("seNamespaces");
            if(!in_array($ns, $namespaces)) return true;

            // only edit windows
            $request = $out->getRequest();
            $action = $request->getText("action");
            if(empty($action) || !in_array($action, ['edit', 'submit'])) return true;

            $interval = $config->get("seInterval");
            $expiration = $config->get("seExpiration");
            $pageid = $title->getArticleID();

            // pass params to js (will be removed)
            $out->mBodytext .= "<div class='d-none' id='safeedits' data-interval='$interval' data-expiration='$expiration' data-pageid='$pageid'></div>";

            $out->addModules('ext.SafeEdits');
            return true;
        }
}
